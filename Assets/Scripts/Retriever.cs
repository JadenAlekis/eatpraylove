﻿using UnityEngine;
using System.Collections;

public class Retriever : MonoBehaviour {

    private Transform bodies;
    private Transform usedBodies;
    
    void Start() {
        bodies = new GameObject("Bodies").transform;
        bodies.SetParent(transform);
        usedBodies = new GameObject("Used Bodies").transform;
        usedBodies.SetParent(transform);
    }
    
    void Update() {
        if (Input.GetKeyDown("1"))
            ExpellBody(transform.position, 1);
    }

    public void RetrieveObject(Transform obj) {
        // assume it's a corpse for now but this could switch and do different things
        obj.transform.SetParent(bodies);
        obj.gameObject.SetActive(false);
        GetComponentInParent<Grower>().Grow();
    }
    
    public int GetBodyCount() {
        int bodyCount = 0;
        for (int i = 0; i < bodies.childCount; i++) {
            if (bodies.GetChild(i) != null)
                bodyCount ++;
        }
        return bodyCount ++;
    }
    
    public int DepositBodies(int count){
        if (count == -1)
            count = 99999;
        int bodyCount = 0;
        for (int i = bodies.childCount - 1; i >= 0; i--) { 
            Transform body = bodies.GetChild(i);
            if (body) {
                body.SetParent(usedBodies);
                GameObject.Destroy(body.gameObject, 2);
                GetComponentInParent<Grower>().Shrink();
                bodyCount ++;
                if (bodyCount >= count)
                    break;
            }
        }
        return bodyCount;
    }
    
    public Transform ExpellBody(Vector2 position, int dir) {
        for (int i = 0; i < bodies.childCount; i++) { 
            Transform body = bodies.GetChild(i);
            if (body) {
                Game.instance.SpawnCorpse(body, position, dir);
                GetComponentInParent<Grower>().Shrink();
                return body;
            }
        }
        return null;
    }
}
