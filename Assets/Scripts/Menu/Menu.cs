﻿using UnityEngine;
using System.Collections;
using InControl;

public class Menu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
    
	void StartGame() {
        Application.LoadLevel("Game");
    }
    
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < InputManager.Devices.Count; i++) {
            InputDevice device = InputManager.Devices[i];
            if (device.MenuWasPressed)
                StartGame();
        }
        
        if (Input.GetKeyDown("enter"))
            StartGame();
	}
}
