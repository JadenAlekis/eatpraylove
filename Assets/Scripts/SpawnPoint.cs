﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

    public Torch[] torches;
    public Transform spawnPosition;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public void LightNextTorch() {
        for (int i = 0; i < torches.Length; i++) {
            if (!torches[i].IsLit()) {
                torches[i].Light();
                return;
            }
        }
    }
    
    public int GetLitTorchCount() {
        int lit = 0;
        for (int i = 0; i < torches.Length; i++) {
            if (torches[i].IsLit())
                lit ++;
        }
        return lit;
    }
    
    public Vector3 GetSpawnPosition() {
        return spawnPosition.position;
    }
}
