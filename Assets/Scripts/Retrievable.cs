﻿using UnityEngine;
using System.Collections;

public class Retrievable : MonoBehaviour {

    public Transform retrievableObject;
    public float unRetrievableTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    unRetrievableTimer -= Time.deltaTime;
	}
    
    public bool IsRetrievable() {
        return unRetrievableTimer <= 0;
    }
    
    public void MakeUnRetrievable(float time) {
        unRetrievableTimer = time;
    }

    void OnTriggerStay2D(Collider2D other) {
        if (!IsRetrievable())
            return;
            
        if (other.tag == "Attack")
        {
            AttackEmitter hitBy = other.GetComponent<AttackEmitter>();
            hitBy.AttackHit(retrievableObject.gameObject);
        }
    }
    
}
