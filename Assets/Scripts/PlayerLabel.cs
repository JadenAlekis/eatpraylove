﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerLabel : MonoBehaviour {

    public Text text;
    public Image bg;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public void SetPlayerID(int playerID) {
        text.color = Color.white;
        text.fontStyle = FontStyle.Bold;
        text.text = "P" + (playerID + 1);
        if (playerID == 0) {
            bg.color = Color.red;
        } else if (playerID == 1) {
            bg.color = Color.blue;
        } else if (playerID == 2) {
            bg.color = Color.green;
        } else if (playerID == 3) {
            bg.color = Color.yellow;
        }
    }
}
