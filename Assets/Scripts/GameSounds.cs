﻿using UnityEngine;
using System.Collections.Generic;

public class GameSounds : MonoBehaviour {

    public static GameSounds instance;

    [System.Serializable]
    public class SoundFiles {        
        public AudioClip eatingHumans;
        public AudioClip groundSlide;
        public AudioClip jumpGrunt;
        public AudioClip kick;
        public AudioClip landingFromHighJump;
        public AudioClip landingFromJump;
        public AudioClip puke;
        public AudioClip punchAndKick;
        public AudioClip punch;
        public AudioClip splashing;
        public AudioClip takeDamage;
        public AudioClip torch;
        public AudioClip wallSlide;
        public AudioClip pointScored;
        public AudioClip victory;
    }
    public SoundFiles sounds; 
    private List<AudioSource> sources;
    public AudioSource sourcePrefab;
    
    void Awake() {
        sources = new List<AudioSource>();
        instance = this;
    }
    
    public void PlayClip(AudioClip clip) {
        AudioSource source = null;
        for (int i = 0; i < sources.Count; i++) {
            if (!sources[i].isPlaying) {
                source = sources[i];
                break;
            }
        }
        if (!source) {
            source = Instantiate(sourcePrefab);
            source.transform.SetParent(transform);
            source.playOnAwake = false;
            sources.Add(source);
        }
        source.clip = clip;
        source.Play();
    }

    public void PlayPointScore()
    {
        PlayClip(sounds.pointScored);
    }

    public void PlayVictory()
    {
        PlayClip(sounds.victory);
    }

    public void PlayEatingHumans() {
        PlayClip(sounds.eatingHumans);
    }
    
    public void PlayGroundSlide() {
    }
    
    public void PlayJumpGrunt() {
        PlayClip(sounds.jumpGrunt);
    }
    
    public void PlayKick() {
        PlayClip(sounds.kick);
    }
    
    public void PlayLandingFromHighJump() {
        PlayClip(sounds.landingFromHighJump);
    }
    
    public void PlayLandingFromJump() {
        PlayClip(sounds.landingFromJump);
    }
    
    public void PlayPuke() {
        PlayClip(sounds.puke);
    }
    
    public void PlayPunchAndKick() {
        PlayClip(sounds.punchAndKick);
    }
    
    public void PlayPunch() {
        PlayClip(sounds.punch);
    }
    
    public void PlaySplashing() {
        PlayClip(sounds.splashing);
    }
    
    public void PlayTakeDamage() {
        PlayClip(sounds.takeDamage);
    }
    
    public void PlayTorch() {
        PlayClip(sounds.torch);
    }
    
    public void PlayWallSlide() {
        PlayClip(sounds.wallSlide);
    }
    
}
