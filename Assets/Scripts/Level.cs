﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

    private Transform spawnPoints;

	// Use this for initialization
	void Awake () {
	    spawnPoints = transform.Find("Spawn Points");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public SpawnPoint GetSpawnPointForPlayer(int player) {
        return spawnPoints.GetChild(player).GetComponent<SpawnPoint>();
    }
}
