﻿using UnityEngine;
using System.Collections;

public class Grower : MonoBehaviour {
    public float amount = 0.25f;
    public float speed = 1;
    private float dest = 1;
    
	// Use this for initialization
	void Start () {
        dest = transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 scale = transform.localScale;
        float newValue = Mathf.MoveTowards(scale.x, dest, Time.deltaTime * speed);
        scale.x = newValue;
        scale.y = newValue;
        transform.localScale = scale;
	}
    
    public void Grow() {
        dest += amount;
    }
    
    public void Shrink() {
        if(!Game.instance.playerSettings.isGodzillaMode)
            dest -= amount;
    }
}
