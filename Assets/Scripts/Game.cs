﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

    public static Game instance;

    public Vector2 corpseForce = new Vector2(5, 5);
    public float corpseUnRetrievableTime = 0.5f;
    public PlayerController.PlayerSettings playerSettings;

    public GameObject corpsePrefab;
    private Transform corpseParent;

    public PlayerController playerPrefab;
    public Level level;
    
    public ParticleSystem bloodSpurt, bloodSpray;
    
    private bool gameIsOver;
    private float endGameTimer;
    public float corpseSpawnMinTime, corpseSpawnMaxTime;
    private float spawnTimer;
    public BoxCollider2D spawnCollider;
    public bool spawnCorpsesInRiver = false;

    public GameObject DebugHitbox;

    void Awake() {
        instance = this;
        corpseParent = new GameObject("Corpses").transform;
        corpseParent.SetParent(transform);
    }
    
    public bool IsGameOver() {
        return gameIsOver;
    }
    
    void Start() {
        SmoothCamera2D sc = FindObjectOfType<SmoothCamera2D>();
        for (int i = 0; i < 4; i++) {
            SpawnPoint sp = level.GetSpawnPointForPlayer(i);
            PlayerController pc = (PlayerController) Instantiate(playerPrefab, 
                    sp.GetSpawnPosition(), Quaternion.identity);
            pc.deviceID = i; // fixme
            pc.playerID = i;
            sc.targets.Add(pc.transform);
        }
    }
    
    void Update() {
        if (gameIsOver) {
            endGameTimer -= Time.deltaTime;
            if (endGameTimer <= 0)
                Application.LoadLevel("Menu");
            return;
        }
        
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0 && spawnCorpsesInRiver) {
            Debug.Log("Spawning corpse");
            float x = Random.Range(spawnCollider.bounds.min.x,
                    spawnCollider.bounds.max.x);
            float y = Random.Range(spawnCollider.bounds.min.y,
                    spawnCollider.bounds.max.y);
            SpawnCorpse(new Vector2(x, y), 0);
            spawnTimer = Random.Range(corpseSpawnMinTime, corpseSpawnMaxTime);
        }
    }
    
    public Vector2 CorpseForce(int dir) {
        Vector2 force = corpseForce;
        force.x *= dir;
        return force;
    }
    
    // spawns a new corpse
    public PlayerController SpawnCorpse(Vector2 position, int dir) {
        GameObject go = (GameObject) Instantiate(corpsePrefab, 
                position, Quaternion.identity);
        PlayerController pc = go.GetComponent<PlayerController>();
        pc.AddExternalForce(CorpseForce(dir), 0);
        pc.transform.SetParent(corpseParent);
        pc.transform.localScale = corpsePrefab.transform.localScale;
        go.GetComponentInChildren<Retrievable>().MakeUnRetrievable(corpseUnRetrievableTime);
        return pc;
    }

    // makes an existing corpse reappear and fly away a bit
    public PlayerController SpawnCorpse(Transform corpse, Vector2 position, int dir) {
        corpse.position = position;
        GameObject go = corpse.gameObject;
        PlayerController pc = go.GetComponent<PlayerController>();
        pc.AddExternalForce(CorpseForce(dir), 0);
        pc.transform.SetParent(corpseParent);
        pc.transform.localScale = corpsePrefab.transform.localScale;
        go.SetActive(true);
        go.GetComponentInChildren<Retrievable>().MakeUnRetrievable(corpseUnRetrievableTime);
        return pc;
    }
    
    public void SpawnBloodSpurt(Vector2 position) {
        ParticleSystem p = (ParticleSystem) GameObject.Instantiate(bloodSpurt, 
                position, Quaternion.identity);
        GameObject.Destroy(p.gameObject, 5);
    }
    
    public void SpawnBloodSpray(Vector2 position) {
        ParticleSystem p = (ParticleSystem) GameObject.Instantiate(bloodSpray, 
                position, Quaternion.identity);
        GameObject.Destroy(p.gameObject, 5);
    }

    public SpawnPoint GetSpawnPointForPlayer(int player) {
        return level.GetSpawnPointForPlayer(player);
    }

    public Vector3 GetSpawnPositionForPlayer(int player) {
        return level.GetSpawnPointForPlayer(player).GetSpawnPosition();
    }
    
    public void EndGame(Transform winner) {
        if (gameIsOver)
            return;
        
        playerSettings.gameMusic.GetComponent<AudioSource>().mute = true;
        GameSounds.instance.PlayVictory();

        SmoothCamera2D sc = FindObjectOfType<SmoothCamera2D>();
        sc.targets.Clear();
        sc.targets.Add(winner);
        sc.offset = Vector3.zero;
        endGameTimer = 8;
        gameIsOver = true;
    }
}
