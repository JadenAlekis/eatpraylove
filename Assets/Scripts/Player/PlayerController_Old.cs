﻿using UnityEngine;
using System.Collections;

public class PlayerController_Old : MonoBehaviour {

    public float speed = 3;
    public Transform graphics;
    public SkeletonAnimation skeleton;

    float x = 0;
    float y = 0;
    Rigidbody2D rb;
    string currentAnimation = "";

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        x = Input.GetAxis("Horizontal") * speed;
        y = Input.GetAxis("Vertical") * speed;

        if (x > 0)
        {
            graphics.localRotation = Quaternion.Euler(0, 180, 0);
            SetAnimation("Walk", true);
        }
        else if (x < 0)
        {
            graphics.localRotation = Quaternion.Euler(0, 0, 0);
            SetAnimation("Walk", true);
        }
        else if (y > 0 || y < 0)
        {
            SetAnimation("Walk", true);
        }
        else
        {
            SetAnimation("Idle", true);
        }
	}

    void SetAnimation(string name, bool loop)
    {
        if (name == currentAnimation)
            return;

        skeleton.state.SetAnimation(0, name, loop);
        currentAnimation = name;
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(x, y);
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }
}
