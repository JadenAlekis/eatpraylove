﻿using UnityEngine;
using System.Collections;
using Prime31;
using InControl;

[RequireComponent(typeof(CharacterController2D))]
public class PlayerController : MonoBehaviour
{

    [System.Serializable]
    public class PlayerSettings
    {
        public float gravity = -25f;
        public float waterFlow = 25f; // for acceleration when in water
        public float waterMaxSpeedDueToFlow = 1f; // limit speed you can be pushed by water 
        public float runSpeed = 8f;
        public float waterRunSpeed = 4f;
        public float groundDamping = 20f; // how fast do we change direction? higher means faster
        public float inAirDamping = 5f;
        public float inWaterDamping = 10f;
        public float jumpHeight = 3f;
        public float wallJumpHorizontalAmount = 3f;
        public float joystickThreshold = 0.25f; // left/right joystick thresholds
        public float joystickJumpDownThreshold = 0.5f; // avoid accidental down presses on the joystick
        public float keyboardEmulatedThreshold = 0.5f;
        public float walkAnimThreshold = 0.1f;
        public float jumpAnimThreshold = 1f;
        public float fallThroughFloorVelocity = -3f;
        public float respawnPenalty = 2f;
        public bool isGodzillaMode;
        public GameObject gameMusic;
    }

    public int deviceID = 0;
    public int playerID;
    public SkeletonAnimation skeleton;
    public Transform graphics;
    public bool ignoreKeyboard;
    public bool ignoreGamepad;

    private CharacterController2D character;
    private Vector3 velocity;
    private PlayerSettings playerSettings;
    private string currentAnimation = "";
    private bool currentLoop;
    private bool isGroundedOnOneWayPlatform;
    private bool leftIsWall, rightIsWall;
    private bool inWater;
    private bool canDoubleJump;
    private int oneWayPlatformLayer, wallLayer, waterLayer;
    private float stunTimer = 0f;
    private Vector2 externalForce;
    public bool isCorpse;
    private float invulnerableTimer;
    private float corpseScale;
    private float dieTimer;

    // Use this for initialization
    void Start()
    {
        playerSettings = Game.instance.playerSettings;
        character = GetComponent<CharacterController2D>();
        character.onControllerCollidedEvent += OnControllerCollided;
        character.onTriggerEnterEvent += OnControllerTriggerEnter;
        character.onTriggerExitEvent += OnControllerTriggerExit;
        oneWayPlatformLayer = LayerMask.NameToLayer("OneWayPlatform");
        wallLayer = LayerMask.NameToLayer("Wall");
        waterLayer = LayerMask.NameToLayer("Water");
        
        if (!isCorpse)
            GetComponentInChildren<PlayerLabel>().SetPlayerID(playerID);
    }
    
    public bool IsGrounded() {
        return character.isGrounded;
    }
    
    public bool IsInvulnerable() {
        return invulnerableTimer > 0;
    }
    
    public void AddHitForce(Vector2 force, float stunTime) {
        if (IsInvulnerable())
            return;
        AddExternalForce(force, stunTime);
        GetHit();
    }

    public void AddExternalForce(Vector2 force, float stunTime) {
        externalForce = force;
        if (stunTime != 0)
            this.stunTimer = stunTime;
    }
    
    void OnControllerCollided(RaycastHit2D rh) {
        // this works (hopefully) because we'll never get a collided hit on a one-way 
        // platform except from below
        if (character.isGrounded && rh.collider.gameObject.layer == oneWayPlatformLayer) {
            isGroundedOnOneWayPlatform = true;
        }
        if (rh.collider.gameObject.layer == wallLayer) {
            if (rh.normal.x == 1)
                leftIsWall = true;
            if (rh.normal.x == -1)
                rightIsWall = true;
        }
    }
    
    void OnControllerTriggerEnter(Collider2D coll) {
        if (coll.gameObject.layer == waterLayer) {
            inWater = true;
        }
    }

    void OnControllerTriggerExit(Collider2D coll) {
        if (coll.gameObject.layer == waterLayer) {
            inWater = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -15) {
            if (isCorpse) {
                GameObject.Destroy(gameObject, 2);
            } else {
                DieAndRespawn(0, false);
            }
        }
        
        invulnerableTimer -= Time.deltaTime;
        stunTimer -= Time.deltaTime;
        bool stunned = stunTimer > 0;
        
        dieTimer -= Time.deltaTime;
        
        // reset state - the call to character.move() will set these again if appropriate
        // via OnControllerCollided()
        isGroundedOnOneWayPlatform = leftIsWall = rightIsWall = false;
        character.move(velocity * Time.deltaTime);
        
        bool isGrounded = character.isGrounded || inWater;
        
        if (isGrounded)
            canDoubleJump = false; // reset double jump ability

        InputDevice device = null;
        try {
            device = InputManager.Devices[deviceID];
        } catch {
        }

        // test code remove me
        // if (device != null && device.Action2.WasPressed && !ignoreGamepad) {
        //     Game.instance.SpawnCorpse(transform.position, transform.localScale.x < 0 ? -1 : 1);
        // }
            
        bool left = false, right = false, down = false, strongDown = false, up = false, isJump = false, wasJump = false;
        float leftValue = 0, rightValue = 0, downValue = 0, upValue = 0;
        if (!isCorpse && !stunned && device != null && !ignoreGamepad) {
            left = device.LeftStick.Left.Value > playerSettings.joystickThreshold;
            right = device.LeftStick.Right.Value > playerSettings.joystickThreshold;
            down = device.LeftStick.Down.Value > playerSettings.joystickThreshold;
            up = device.LeftStick.Up.Value > playerSettings.joystickThreshold;
            if (left)
                leftValue = device.LeftStick.Left.Value;
            if (right)
                rightValue = device.LeftStick.Right.Value;
            if (down) {
                downValue = device.LeftStick.Down.Value;
                // keep getting false positives on pressing down so make the joystick threshold big
                strongDown = downValue > playerSettings.joystickJumpDownThreshold;
            }
            if (up)
                upValue = device.LeftStick.Up.Value;
            if (device.Action1.IsPressed)
                isJump = true;
            if (device.Action1.WasPressed)
                wasJump = true;
        }
        if (!isCorpse && !stunned && !ignoreKeyboard) {
            if (Input.GetKey(KeyCode.LeftArrow)) {
                left = true;
                leftValue = playerSettings.keyboardEmulatedThreshold;
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                right = true;
                rightValue = playerSettings.keyboardEmulatedThreshold;
            }
            if (Input.GetKey(KeyCode.DownArrow)) {
                down = true;
                downValue = playerSettings.keyboardEmulatedThreshold;
                strongDown = true;
            }
            if (Input.GetKey(KeyCode.UpArrow)) {
                up = true;
                upValue = playerSettings.keyboardEmulatedThreshold;
            }
            if (Input.GetKey(KeyCode.Space))
                isJump = true;
            if (Input.GetKeyDown(KeyCode.Space))
                wasJump = true;
        }
                
        // grab our current velocity to use as a base for all calculations
        velocity = character.velocity;

        velocity += (Vector3) externalForce;
        externalForce = Vector2.zero;

        if (isGrounded)
            velocity.y = 0;
            
        // movement due to walking
        float normalizedHorizontalSpeed = 0, normalizedVerticalSpeed = 0;
        if (right) {
            normalizedHorizontalSpeed = rightValue;
            graphics.localRotation = Quaternion.Euler(0, 0, 0);
        } else if (left) {
            normalizedHorizontalSpeed = -leftValue;
            graphics.localRotation = Quaternion.Euler(0, 180, 0);
        }
        // vertical speed is only used when in water
        if (up) {
            normalizedVerticalSpeed = upValue;
        } else if (down) {
            normalizedVerticalSpeed = -downValue; 
        }
        
        // we can only jump whilst grounded
        if (isGrounded && wasJump && (!(isGroundedOnOneWayPlatform && strongDown))) {
            // normal jump off the ground
            velocity.y = Mathf.Sqrt(2f * playerSettings.jumpHeight * -playerSettings.gravity);
            canDoubleJump = true;
            inWater = false; // allows us to jump out of water
            GameSounds.instance.PlayJumpGrunt();
            // _animator.Play( Animator.StringToHash( "Jump" ) );
        } else if (!isGrounded && wasJump && (leftIsWall || rightIsWall)) {
            // wall jump
            velocity.y = Mathf.Sqrt(2f * playerSettings.jumpHeight * -playerSettings.gravity);
            if (leftIsWall) {
                velocity.x = playerSettings.wallJumpHorizontalAmount;
            } else if (rightIsWall) {
                velocity.x = -playerSettings.wallJumpHorizontalAmount;
            }
            GameSounds.instance.PlayJumpGrunt();
        } else if (wasJump && !isGrounded && canDoubleJump) {
            velocity.y = Mathf.Sqrt(2f * playerSettings.jumpHeight * -playerSettings.gravity);
            canDoubleJump = false;
            GameSounds.instance.PlayJumpGrunt();
        }

        // apply intentional forces
        // apply speed smoothing. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = isGrounded ? playerSettings.groundDamping : playerSettings.inAirDamping; // how fast do we change direction?
        if (inWater) {
            velocity.x = Mathf.Lerp(velocity.x, normalizedHorizontalSpeed * playerSettings.waterRunSpeed,
                    Time.deltaTime * smoothedMovementFactor);
            velocity.y = Mathf.Lerp(velocity.y, normalizedVerticalSpeed * playerSettings.waterRunSpeed,
                    Time.deltaTime * smoothedMovementFactor);
        } else {
            velocity.x = Mathf.Lerp(velocity.x, normalizedHorizontalSpeed * playerSettings.runSpeed,
                    Time.deltaTime * smoothedMovementFactor);
        }

        // apply physics forces like gravity and water flow
        if (!inWater) {
            velocity.y += playerSettings.gravity * Time.deltaTime;
        } else {
            if (Mathf.Abs(velocity.x) < playerSettings.waterMaxSpeedDueToFlow) 
                velocity.x += playerSettings.waterFlow * Time.deltaTime;
        }

        // if holding down bump up our movement amount and turn off one way platform detection for a frame.
        // this lets us jump down through one way platforms
        if (isGroundedOnOneWayPlatform && strongDown && wasJump)
        {
            // accelerate down a bit so we def. fall through the floor
            if (velocity.y > playerSettings.fallThroughFloorVelocity)
                velocity.y = playerSettings.fallThroughFloorVelocity;
            character.ignoreOneWayPlatformsThisFrame = true;
        }

        if (dieTimer > 0)
            return;
        if (!isCorpse && Game.instance.IsGameOver())
        {
            SetAnimation("JumpUp", true);
            return;
        }
        else
        {
            if (!isCorpse && !GetComponent<AttackManager>().IsAttacking())
            {
                if (velocity.y > playerSettings.jumpAnimThreshold)
                {
                    SetAnimation("JumpIdle", false);
                }
                else if (velocity.y < -playerSettings.jumpAnimThreshold)
                {
                    SetAnimation("JumpLand", false);
                }
                else
                {
                    if (velocity.x > playerSettings.walkAnimThreshold || velocity.x < -playerSettings.walkAnimThreshold)
                        SetAnimation("Run", true);
                    else if (currentAnimation != "")
                        SetAnimation("Idle", true);
                }
            }
        }
    }

    public void SetAnimation(string name, bool loop)
    {
        if (name == currentAnimation && loop == currentLoop)
            return;

        skeleton.state.SetAnimation(0, name, loop);
        currentAnimation = name;
        currentLoop = loop;
    }
    
    public int DepositBodies(int count) {
        /*if (!character.isGrounded)
            return 0;*/
        Retriever r = GetComponentInChildren<Retriever>();
        return r.DepositBodies(count);
    }
    
    public void GetHit() {
        if (isCorpse)
            return;
        if (IsInvulnerable())
            return;
        invulnerableTimer = 2;
        Retriever retriever = GetComponentInChildren<Retriever>();
        int bodyShootDir = velocity.x > 0 ? 1 : -1; // fixme
        // if (retriever.ExpellBody(transform.position, bodyShootDir) == null) {
        if (retriever.GetBodyCount() == 0) {
            DieAndRespawn(bodyShootDir, true);
        } else {
            DepositBodies(1);
            SetAnimation("DieBackwards", false);
            stunTimer = 2;
            dieTimer = 2;
            // DieAndRespawn(bodyShootDir, true);
        }
    }
    
    public void DieAndRespawn(int dir, bool spawnCorpse) {
        if (isCorpse)
            return;
        GetComponentInChildren<Retriever>().DepositBodies(-1);
        if (spawnCorpse) {
            PlayerController pc = Game.instance.SpawnCorpse(transform.position, dir);
            pc.velocity = velocity;
        }
        externalForce = Vector2.zero;
        velocity = Vector3.zero;
        transform.position = Game.instance.GetSpawnPositionForPlayer(playerID);
        invulnerableTimer = playerSettings.respawnPenalty;
        GetComponent<Blinker>().timeToBlink = playerSettings.respawnPenalty;
    }
}
