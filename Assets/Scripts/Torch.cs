﻿using UnityEngine;
using System.Collections;

public class Torch : MonoBehaviour {
    
    public GameObject flame;
    private bool lit;

	// Use this for initialization
	void Start () {
        lit = false;
	    flame.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public void Light() {
        lit = true;
        flame.SetActive(true);
        GameSounds.instance.PlayTorch();
    }
    
    public bool IsLit() {
        return lit;
    }
}
