﻿using UnityEngine;
using System.Collections;

public class Blinker : MonoBehaviour {

    public GameObject objectToBlink;
    public float interval;
    public float timeToBlink;
    private float intervalTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        timeToBlink -= Time.deltaTime;
	    if (timeToBlink > 0) {
            intervalTimer += Time.deltaTime;
            if (intervalTimer >= interval) {
                if (objectToBlink.activeInHierarchy)
                    objectToBlink.SetActive(false);
                else
                    objectToBlink.SetActive(true);
                intervalTimer = 0;
            }
        } else if (timeToBlink < 0) {
            objectToBlink.SetActive(true);
            timeToBlink = 0;
        }
	}
}
