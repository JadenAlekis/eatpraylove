﻿using UnityEngine;
using System.Collections;
using Prime31;

public class BasicAttack : Attack
{

    [HideInInspector]
    GameObject attacker;

    GameObject hitbox;

    public BasicAttack(GameObject _attacker)
    {
        attacker = _attacker;
    }

    // Use this for initialization
    public override void Start()
    {
        chargeTime = 0.1f;
        attackTime = 0.3f;
        cooldownTime = 0.2f;

        stunTime = 0.5f;
        forceFactor = 30f;

        attackerPC = attacker.GetComponent<PlayerController>();

        if (attacker.GetComponent<CharacterController2D>().isGrounded)
        {
            attackerPC.SetAnimation("Punch Forward", false);
        }
        else
        {
            attackerPC.SetAnimation("JumpKick", false);
        }

        if (Game.instance.DebugHitbox != null)
        {
            //hitbox = (GameObject)GameObject.Instantiate(Resources.Load("HitBox"), attacker.GetComponent<AttackManager>().RightArm.transform.position, attacker.GetComponent<AttackManager>().RightArm.transform.rotation);
            //hitbox.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0, 0);
        }

        GameSounds.instance.PlayPunchAndKick();
    }

    // Update is called once per frame
    public override void Update()
    {
        timeAlive += Time.deltaTime;

        if (hitbox != null)
            hitbox.transform.position = attacker.GetComponent<AttackManager>().RightArm.transform.position;

        switch (currentPhase)
        {
            case AttackPhase.Charge:
                if (timeAlive > chargeTime)
                {
                    if (hitbox != null)
                    hitbox.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0, 127);
                    currentPhase = AttackPhase.Active;
                    timeAlive = 0;
                }
                break;
            case AttackPhase.Active:
                if (timeAlive > attackTime)
                {
                    if (hitbox != null)
                    hitbox.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0, 0);
                    currentPhase = AttackPhase.Cooldown;
                    timeAlive = 0;
                }
                break;
            case AttackPhase.Cooldown:
                if (timeAlive > cooldownTime)
                {
                    if (hitbox != null)
                        GameObject.Destroy(hitbox);
                    isFinished = true;
                }
                break;
            default:
                break;
        }
    }

    public override void Hit(GameObject victim)
    {
        if (currentPhase == AttackPhase.Active)
        {
            currentPhase = AttackPhase.Cooldown;

            PlayerController pc = victim.GetComponent<PlayerController>();
            pc.AddHitForce((victim.transform.position - attacker.transform.position).normalized * forceFactor, stunTime);

            // Game.instance.SpawnBloodSpray(victim.transform.position);
            GameSounds.instance.PlayPunch();
        }
    }
}
