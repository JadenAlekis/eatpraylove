﻿using UnityEngine;
using System.Collections;
using Prime31;

public class Cannibalize : Attack {

    [HideInInspector]
    GameObject attacker;
    public bool spawnedBlood;

    public Cannibalize(GameObject _attacker)
    {
        attacker = _attacker;
    }

	// Use this for initialization
	public override void Start () {
        chargeTime = 0.50f;
        attackTime = 0.50f;
        cooldownTime = 0f;

        attackerPC = attacker.GetComponent<PlayerController>();
        attackerPC.SetAnimation("Cannibalize", false);
        attackerPC.AddExternalForce(Vector2.zero, chargeTime + attackTime);
        GameSounds.instance.PlayEatingHumans();
	}
	
	// Update is called once per frame
	public override void Update () {
        timeAlive += Time.deltaTime;

        switch (currentPhase)
        {
            case AttackPhase.Charge:
                if (timeAlive > chargeTime)
                {
                    currentPhase = AttackPhase.Active;
                    timeAlive = 0;
                }
                break;
            case AttackPhase.Active:
                if (timeAlive > attackTime)
                {
                    currentPhase = AttackPhase.Cooldown;
                    timeAlive = 0;
                }
                break;
            case AttackPhase.Cooldown:
                if (timeAlive > cooldownTime)
                    isFinished = true;
                break;
            default:
                break;
        }
	}

    public override void Hit(GameObject victim)
    {
        if (currentPhase == AttackPhase.Charge) {
            if (!spawnedBlood) 
                Game.instance.SpawnBloodSpurt(victim.transform.position);
            spawnedBlood = true;                
        }
        
        if (currentPhase == AttackPhase.Active)
        {
            currentPhase = AttackPhase.Cooldown;

            Debug.Log("Cannibalize Action Trigger: " + victim.GetComponentInParent<PlayerController>());

            if (victim.GetComponent<PlayerController>().isCorpse)
            {
                Retriever retriver = attacker.GetComponentInChildren<Retriever>();
                Debug.Log(retriver);
                retriver.RetrieveObject(victim.transform);
            }
            //attacker.GetComponent<Retriever>().RetrieveObject(victim.transform);

            /*PlayerController pc = victim.GetComponent<PlayerController>();
            pc.AddHitForce((victim.transform.position - attacker.transform.position).normalized * forceFactor, stunTime);*/
        }
    }
}
