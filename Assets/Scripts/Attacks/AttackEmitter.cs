﻿using UnityEngine;
using System.Collections;

public class AttackEmitter : MonoBehaviour {

    public GameObject attacker;

    private Attack activeAttack;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (activeAttack != null)
        {
            activeAttack.Update();
            if (activeAttack.isFinished)
                activeAttack = null;
        }
	}

    public bool IsActive()
    {
        return activeAttack != null;
    }

    public void SetAttack(Attack desiredAttack)
    {
        if (activeAttack == null || activeAttack.isFinished)
        {
            activeAttack = desiredAttack;
            activeAttack.Start();
        }
    }

    public void AttackHit(GameObject victim)
    {
        if (activeAttack != null)
        {
            activeAttack.Hit(victim);
        }
    }
}
