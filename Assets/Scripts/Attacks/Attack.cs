﻿using UnityEngine;
using System.Collections;

public abstract class Attack {

    public bool isFinished;

    protected float timeAlive;

    protected float chargeTime;
    protected float attackTime;
    protected float cooldownTime;

    protected float stunTime;
    protected float forceFactor;

    protected PlayerController attackerPC;

    protected AttackPhase currentPhase;

    public abstract void Start();
    public abstract void Update();

    public abstract void Hit(GameObject victim);
}

public enum AttackPhase
{
    Charge,
    Active,
    Cooldown
}