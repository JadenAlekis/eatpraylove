﻿using UnityEngine;
using System.Collections;
using InControl;
using Prime31;

public class AttackManager : MonoBehaviour {

    public GameObject RightArm;
    public GameObject LeftArm;
    public GameObject RightLeg;
    public GameObject LeftLeg;

    AttackEmitter leftArmEmitter;
    AttackEmitter rightArmEmitter;
    AttackEmitter leftLegEmitter;
    AttackEmitter rightLegEmitter;

    PlayerController player;
    int controllerID = 0;

    private PlayerController.PlayerSettings playerSettings;

	// Use this for initialization
	void Start () {
        leftArmEmitter = LeftArm.GetComponent<AttackEmitter>();
        rightArmEmitter = RightArm.GetComponent<AttackEmitter>();
        leftLegEmitter = LeftArm.GetComponent<AttackEmitter>();
        rightLegEmitter = RightArm.GetComponent<AttackEmitter>();

        controllerID = GetComponent<PlayerController>().deviceID;

        player = GetComponent<PlayerController>();

        playerSettings = Game.instance.playerSettings;
	}
	
	// Update is called once per frame
	void Update () {
        InputDevice device = null;
        
        try {
            device = InputManager.Devices[controllerID];
        } catch {
            return;
        }

        if (!IsAttacking())
        {
            if (device.Action3.IsPressed
                    && device.LeftStick.Up.Value > playerSettings.joystickThreshold)
            {
                leftArmEmitter.SetAttack(new BasicAttack(this.gameObject));
            }
            else if (device.Action4.IsPressed)
            {
                leftArmEmitter.SetAttack(new Cannibalize(this.gameObject));
            }
            else if (device.Action3.IsPressed)
            {
                leftArmEmitter.SetAttack(new BasicAttack(this.gameObject));
            }
        }
	}

    public bool IsAttacking()
    {
        return leftArmEmitter.IsActive() || rightArmEmitter.IsActive() || leftLegEmitter.IsActive() || rightArmEmitter.IsActive();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Attack")
        {
            AttackEmitter hitBy = other.GetComponent<AttackEmitter>();
            hitBy.AttackHit(this.gameObject);
        }
    }
}
