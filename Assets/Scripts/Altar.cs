﻿using UnityEngine;
using System.Collections;

public class Altar : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnTriggerStay2D(Collider2D other) {
        if (Game.instance.IsGameOver())
            return;
        Retriever retriever = other.gameObject.GetComponent<Retriever>();
        if (retriever == null)
            return;
        PlayerController pc = retriever.GetComponentInParent<PlayerController>();
        if (!pc)
            return;
        if (pc.IsGrounded()) {
            int count = pc.DepositBodies(-1);

            if (count > 0)
                GameSounds.instance.PlayPointScore();

            SpawnPoint sp = Game.instance.GetSpawnPointForPlayer(pc.playerID);
            for (int i = 0; i < count; i++)
                sp.LightNextTorch();
            if (sp.GetLitTorchCount() == 4) {
                Game.instance.EndGame(pc.transform);
            }
            pc.DieAndRespawn(0, false);
        }
    }    
}
