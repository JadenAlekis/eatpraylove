﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SmoothCamera2D : MonoBehaviour
{
    public float dampTime = 0.15f;
    public List<Transform> targets;

    private Vector3 velocity = Vector3.zero;
    private Camera camera;
    public Vector3 offset = new Vector3(0f, 3f, 0f);

    private Vector3 target = new Vector3();

    const float MAX_ZOOM_IN = 2f;
    const float MAX_ZOOM_OUT = 10f;
    const float ZOOM_SCALE = 1f;
    const float ZOOM_SPEED = 1f;

    protected float _zoom;
    public float zoom
    {
        get { return _zoom; }
        set
        {
            _zoom = value > MAX_ZOOM_OUT ? MAX_ZOOM_OUT : value < MAX_ZOOM_IN ? MAX_ZOOM_IN : value;
        }
    }
    protected float targetZoom;

    // Use this for initialization
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (targets != null && targets.Count > 0)
        {
            target = SetAveragePosition(targets);

            target += offset;

            Vector3 point = camera.WorldToViewportPoint(target);
            Vector3 delta = target - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            zoom += (targetZoom - zoom) * ZOOM_SPEED * Time.deltaTime;

            camera.orthographicSize = zoom;
        }

    }

    Vector3 SetAveragePosition(List<Transform> _targets)
    {
        Vector3 average = new Vector3();
        double farthest = 0d;

        int count = 0;

        foreach (var target in targets)
        {
                average += target.position;
                count++;

        }

        if (count != 0)
        {
            average /= count;

            foreach (var target in targets)
            {
                    double testDistance = Math.Sqrt(Math.Pow(average.x - target.position.x, 2) + Math.Pow(average.y - target.position.y, 2));
                    if (testDistance > farthest)
                        farthest = testDistance;
            }

            targetZoom = (float)farthest * ZOOM_SCALE;
            return average;
        }
        else
        {
            targetZoom = 3f;
            return average;
        }
    }
}
