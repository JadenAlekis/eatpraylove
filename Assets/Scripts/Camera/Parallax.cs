﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

    public float ParallaxFactor = 0.2f;

    private Vector3 offset;

	// Use this for initialization
	void Start () {
        offset = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3((transform.parent.position.x + offset.x) * ParallaxFactor, (transform.parent.position.y + offset.y) * ParallaxFactor, offset.z);
    }
}
